(function($) {

  var initialized;

  Drupal.behaviors.combinary = {
    attach: function (context, settings) {
      initShuffleOnlyOnce(context, settings);
    }
  };

  function initShuffleOnlyOnce(context, settings) {
    if (!initialized) {
      initialized = true;

      var Shuffle = window.Shuffle;
      var gridContainerElement = document.querySelectorAll('section.combinary');
      var combinaryBlockId = settings['combinary']['combinary_block_id'];
      var currentOffset = parseInt(settings['combinary']['combinary_items_limit']);
      var loadMorePosts = parseInt(settings['combinary']['combinary_load_more_posts']);
      var shuffleInstance = [];
      var filterItems = Object.keys(settings['snetworks']);

      class CombinaryShuffle {
        constructor(element, gridContainer, array_pos) {
          this.element = element;
          this.gridContainer = gridContainer;
          this.array_pos = array_pos;
          shuffleInstance[this.array_pos] = new Shuffle(element, {
            itemSelector: '.combinary__item',
            //sizer: element.querySelector('.my-sizer-element'),
          });

          this.addAjaxLoadMore();

          // Log events.
          this.addShuffleEventListeners();
          //this._activeFilters = [];
          this.addFilterButtons();
          this.addSearchFilter();

        }

        /**
         * Shuffle uses the CustomEvent constructor to dispatch events. You can listen
         * for them like you normally would (with jQuery for example).
         */
        addShuffleEventListeners() {
          shuffleInstance[this.array_pos].on(Shuffle.EventType.LAYOUT, (data) => {
            //console.log('layout. data:', data);
          });
          shuffleInstance[this.array_pos].on(Shuffle.EventType.REMOVED, (data) => {
            //console.log('removed. data:', data);
          });
        }

        addFilterButtons() {
          const options = this.gridContainer.querySelector('.combinary__filter-options');
          //console.log(options);
          if (!options) {
            return;
          }

          const filterButtons = Array.from(options.children);
          const onClick = this._handleFilterClick.bind(this);
          filterButtons.forEach((button) => {
            button.addEventListener('click', onClick, false);
          });
        }

        _handleFilterClick(evt) {
          //console.log(evt);
          const btn = evt.currentTarget;
          const isActive = btn.classList.contains('active');
          const btnGroup = btn.getAttribute('data-group');

          this._removeActiveClassFromChildren(btn.parentNode);

          let filterGroup;
          if (isActive) {
            btn.classList.remove('active');
            filterGroup = Shuffle.ALL_ITEMS;
          } else {
            btn.classList.add('active');
            filterGroup = btnGroup;
          }

          shuffleInstance[this.array_pos].filter(filterGroup);
        }

        _removeActiveClassFromChildren(parent) {
          const { children } = parent;
          for (let i = children.length - 1; i >= 0; i--) {
            children[i].classList.remove('active');
          }
        }

        // Advanced filtering
        addSearchFilter() {
          const searchInput = this.gridContainer.querySelector('.js-combinary-shuffle-search');
          if (!searchInput) {
            return;
          }
          searchInput.addEventListener('keyup', this._handleSearchKeyup.bind(this));
        }

        /**
         * Filter the shuffle instance by items with a title that matches the search input.
         * @param {Event} evt Event object.
         */
        _handleSearchKeyup(evt) {
          const searchText = evt.target.value.toLowerCase();
          shuffleInstance[this.array_pos].filter((element, shuffle) => {
            // If there is a current filter applied, ignore elements that don't match it.
            if (shuffle.group !== Shuffle.ALL_ITEMS) {
              // Get the item's groups.
              const groups = JSON.parse(element.getAttribute('data-groups'));
              const isElementInCurrentGroup = groups.indexOf(shuffle.group) !== -1;
              // Only search elements in the current group
              if (!isElementInCurrentGroup) {
                return false;
              }
            }
            //const titleElement = element.querySelector('.combinary__title');
            //const titleText = titleElement.textContent.toLowerCase().trim();
            const contentElement = element.querySelector('.combinary__text');
            const contentText = contentElement.textContent.toLowerCase().trim();
            //return contentText.indexOf(searchText) !== -1 || titleText.indexOf(searchText) !== -1 ;
            return contentText.indexOf(searchText) !== -1;
          });
        }

        // Load more feature
        addAjaxLoadMore() {
          const ajaxLoadMore = this.gridContainer.querySelector('.combinary-load-more-button');
          if (!ajaxLoadMore) {
            return;
          }
          ajaxLoadMore.addEventListener('click', this._fetchNextPage.bind(this));
        }

        _fetchNextPage() {

          var feed_url = '/combinarylist/' + combinaryBlockId + '/' + loadMorePosts + '/' + currentOffset;
          currentOffset += loadMorePosts;

          var that = this;

          fetch( feed_url )
            .then(function (response) {
              return response.text();
            })
            .then(function (html) {
              //console.log(html);

              // Convert the HTML string into a document object
              var parser = new DOMParser();
              var resp = parser.parseFromString(html, 'text/html');

              var articles = resp.getElementsByTagName("article");
              var elements = Array.from(articles);

              elements.forEach(function(element) {
                shuffleInstance[that.array_pos].element.appendChild(element);

                if(!filterItems.includes(element.dataset.filter) && (element.dataset.filter != '')) {
                  var new_item = element.dataset.filter;
                  filterItems.push(new_item);

                  var btn = document.createElement("button");   // Create a <button> element
                  btn.innerHTML = new_item;
                  btn.classList.add('combinary__filter-btn');
                  btn.setAttribute("data-group", new_item);

                  var filterOptions = that.gridContainer.querySelector('.combinary__filter-options');
                  filterOptions.appendChild(btn);
                  filterOptions.append(" ");

                  btn.addEventListener('click', that._handleFilterClick.bind(that), false);

                }

              }, this);

              shuffleInstance[that.array_pos].add(elements);

            });

        };

      }

      gridContainerElement.forEach(function(gridContainer, i) {
        var shuffleElement = gridContainer.querySelector('.js-combinary-items');
        context.demo = new CombinaryShuffle(shuffleElement, gridContainer, i);
      });

    }
  }
})();
