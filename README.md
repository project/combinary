<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

#Combinary
Drupal Module
https://www.drupal.org/project/combinary

Details about the project
https://www.acolono.com/combinary

##ABOUT
Combinary is an open source suite with which social media activities
and structured data can be saved, analyzed and presented.

With this Drupal Module you can embed an Combinary Stream easily to your Site.
The json stream contains all information to display Social Media posts like
Facebook, Twitter, Youtube or even RSS items. You can use the provided Twig
template to modify and theme them like you want.

## DEVELOPMENT

Please do `npm install` in order to install gulp, etc...
Available gulp commands are `gulp watch` and `gulp sass`

## ISSUES
https://www.drupal.org/project/issues/combinary?categories=All

## NEWS & COMMUNICATE
twitter: https://twitter.com/ao_combinary


##by acolono GmbH
~~we build your websites~~
we build you business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
