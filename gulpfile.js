// Initialize modules
var gulp = require('gulp');
var sass = require('gulp-sass');

// Sass task: compiles the combinary.scss file into combinary.css
gulp.task('sass', function(){
  return gulp.src('css/combinary.scss')
    .pipe(sass({outputStyle: 'expanded'})) // compile SCSS to CSS
    .pipe(gulp.dest('css')); // put final CSS in dist folder
});

// Watch task: watch SCSS and JS files for changes
gulp.task('watch', function(){
  gulp.watch('css/*.scss', gulp.series('sass'));
});

// Default task
gulp.task('default', gulp.series('sass', 'watch'));
