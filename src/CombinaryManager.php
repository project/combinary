<?php

namespace Drupal\combinary;

use GuzzleHttp\Client;

class CombinaryManager {

  public function getCombinaryJson($config, $limit, $offset) {

    $posts = [];

    try {
      $server_url = $config['combinary_url'];
      $client = new Client(['base_uri' => $server_url]);
      $request = $client->request("GET", '', [
        'query' => [
          'limit' => $limit,
          'offset' => $offset
        ]
      ]);
      $status = $request->getStatusCode();

      switch ($status) {
        case 200:
          $posts = json_decode($request->getBody()->getContents(), TRUE);
          break;
      }
    } catch (\Exception $e) {

    }

    return $posts;
  }

}
