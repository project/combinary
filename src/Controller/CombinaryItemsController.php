<?php

/**
 * CombinaryItemsController.php file.
 */

namespace Drupal\combinary\Controller;

use Drupal\Core\Controller\ControllerBase;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\combinary\CombinaryManager;

/**
 * CombinaryItemsController - custom controller created for use with AJAX call.
 */
class CombinaryItemsController extends ControllerBase {

  protected $manager;

  /**
   * CombinaryItemsController constructor.
   */
  public function __construct(CombinaryManager $combinary_manager) {
    $this->manager = $combinary_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('combinary')
    );
  }

  /**
   * Render posts into template.
   */
  public function render($combinary_block_id, $limit, $offset) {

    $block = \Drupal\block\Entity\Block::load( $combinary_block_id );
    if ($block) {
      $config = $block->get('settings');
    }

    $posts = $this->manager->getCombinaryJson($config, $limit, $offset);

    $build = [
      '#theme' => 'combinary_list_theme_hook',
      '#entries' => $posts,
      '#settings' => $config,
    ];

    return new Response(render($build));

  }

}
