<?php

/**
 * CombinaryBlock.php file.
 */

namespace Drupal\combinary\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\combinary\CombinaryManager;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'combinary' Block.
 *
 * @Block(
 * id = "block_combinary",
 * admin_label = @Translation("Combinary"),
 * )
 */
class CombinaryBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $manager;

  /**
   * CombinaryBlock constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CombinaryManager $combinary_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->manager = $combinary_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('combinary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['combinary_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Combinary JSON file URL'),
      '#description' => $this->t('Enter your Combinary JSON file URL'),
      '#default_value' => isset($config['combinary_url']) ? $config['combinary_url'] : '',
      '#required' => TRUE,
    ];
    $form['combinary_items_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Initial item amount'),
      '#description' => $this->t('Max number of items loaded'),
      '#default_value' => isset($config['combinary_items_limit']) ? $config['combinary_items_limit'] : 10,
      '#required' => TRUE,
    ];
    $form['combinary_load_more_posts'] = [
      '#type' => 'number',
      '#title' => $this->t('Load more items amount'),
      '#description' => $this->t('Load more - number of posts per load'),
      '#default_value' => isset($config['combinary_load_more_posts']) ? $config['combinary_load_more_posts'] : 10,
      '#required' => TRUE,
    ];
    $form['combinary_striptags'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Strip html tags'),
      '#default_value' => isset($config['combinary_striptags']) ? $config['combinary_striptags'] : '',
    ];
    $form['combinary_limit_post_characters'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Limit characters'),
      '#description' => $this->t('Limit post characters'),
      '#default_value' => isset($config['combinary_limit_post_characters']) ? $config['combinary_limit_post_characters'] : '',
    ];

    $form['combinary_show_search'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show search bar?'),
      '#default_value' => isset($config['combinary_show_search']) ? $config['combinary_show_search'] : FALSE,
    ];
    $form['combinary_show_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show filter bar?'),
      '#default_value' => isset($config['combinary_show_filter']) ? $config['combinary_show_filter'] : TRUE,
    ];
    $form['combinary_load_more'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show load more button?'),
      '#default_value' => isset($config['combinary_load_more']) ? $config['combinary_load_more'] : TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();

    $this->configuration['combinary_url'] = $values['combinary_url'];
    $this->configuration['combinary_items_limit'] = $values['combinary_items_limit'];
    $this->configuration['combinary_load_more_posts'] = $values['combinary_load_more_posts'];
    $this->configuration['combinary_striptags'] = $values['combinary_striptags'];
    $this->configuration['combinary_limit_post_characters'] = $values['combinary_limit_post_characters'];
    $this->configuration['combinary_show_search'] = $values['combinary_show_search'];
    $this->configuration['combinary_show_filter'] = $values['combinary_show_filter'];
    $this->configuration['combinary_load_more'] = $values['combinary_load_more'];

  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $posts = $this->manager->getCombinaryJson($config, $config['combinary_items_limit'], 0);

    $entries = [];
    $snetworks = [];

    foreach ($posts as $post) {
      $entries[] = $post;
      $snetworks[$post['itemType']] = $post['itemType'];
    }

    return [
      '#theme' => 'combinaryblock-block',
      '#entries' => $entries,
      '#snetworks' => $snetworks,
      '#settings' => $config,
      '#attached' => [
        'library' => ['combinary/combinary-css-and-js'],
        'drupalSettings' => [
          'snetworks' => $snetworks,
        ],
      ],
    ];
  }
}
